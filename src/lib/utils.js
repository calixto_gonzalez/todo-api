function getStandardErrorFields(path) {
    return {
        timestamp: new Date().toISOString(),
        path: path
    };
}

function isEmpty(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

module.exports = {
    getStandardErrorFields: getStandardErrorFields,
    isEmpty: isEmpty
};