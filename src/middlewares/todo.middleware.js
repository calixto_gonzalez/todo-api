const allowedStates = ['pending', 'done', 'in-progress'];
const validator = require('validator');
const {getStandardErrorFields} = require("../lib/utils");
exports.checkState = function (req, res, next) {
    console.log(`Checking state middleware for state: ${req.body.state}`);
    if (req.body.state && allowedStates.includes(String(req.body.state).toLowerCase())) {
        next()
    } else {
        res.status(400).json({
            message: `State is not : ${allowedStates}`,
            ...getStandardErrorFields(req.path)
        });
    }
};

exports.checkID = function (req, res, next) {
    console.log(`Checking ID middleware id: ${req.params.id}`);
    if (req.params.id && validator.isNumeric(req.params.id)) {
        next()
    }
    else {
        res.status(400).json({
            message: `ID is not valid or present in the URL`,
            ...getStandardErrorFields(req.path)
        })
    }

}

exports.checkDescription = function (req, res, next) {
    console.log(`Checking description middleware for ID: ${req.params.id}`)
    if (!validator.isEmpty(req.body.description)) {
        next();
    } else {
        res.status(400).json({
            message: `Description isn't present in body`,
            ...getStandardErrorFields(req.path)
        })
    }
}
