const {Todo} = require('../models/todo.model');
const fs = require('fs');
const path = require('path');
const {getStandardErrorFields, isEmpty} = require("../lib/utils");
const selectedFields = {
    description: 1,
    state: 1,
    _id: 1,
    createdAt: 1,
    "attachment.path": 1,
    "attachment.contentType": 1,
    "attachment.createdAt": 1
};

function todoNotFoundResponse(res) {
    console.log("No records were found");
    res.status(200).json({
        message: 'TODO not found'
    })
}

exports.create = function (req, res, next) {
    console.log("Creating TODO");
    let attachment;
    if (req.file) {
        attachment = {
            name: req.file.filename,
            path: req.file.path,
            contentType: req.file.mimetype
        }
    }
    const todo = new Todo({
        description: req.body.description.trim(),
        state: req.body.state,
        attachment: attachment
    })


    todo.save()
        .then(result => {
            console.log("TODO created successfully");
            res.status(201).json({
                message: "Created TODO successfully",
                body: {
                    todo: result
                }
            })
        })
        .catch(error => {
            console.error(error);
            res.status(500).json({message: error.message, ...getStandardErrorFields(req.path)})
        });

};
exports.getAttachment = function (req, res, next) {
    console.log(`Trying to find attachment by id: ${req.params.id}`);
    Todo.findById(req.params.id)
        .then(doc => {
            if (doc) {
                res.sendFile(doc.attachment.name, {root: path.join(__dirname,'../public/files/')});
            } else {
                todoNotFoundResponse(res);
            }
        }).catch(error => {
        console.error(error);
        res.status(500).json({message: error.message, ...getStandardErrorFields(req.path)})
    });
};

exports.get = function (req, res, next) {
    let filter = {
        description: req.query.description,
        state: req.query.state,
        _id: req.query.id || req.params.id
    };
    Object.keys(filter).forEach(key => filter[key] === undefined && delete filter[key]);
    console.log(`Trying to find by filter: ${isEmpty(filter) ? 'no filters set getting ALL TODOs' : filter}`);
    Todo.find(filter, selectedFields)
        .then(docs => {
            let response = {
                message: "TODOs found",
                body: {
                    count: docs.length,
                    todos: docs.map(doc => {
                        return doc
                    })
                }
            };
            if (docs.length !== 0) {
                console.log("Found records");
                res.status(200).json(response)
            } else {
                todoNotFoundResponse(res)
            }
        }).catch(error => {
        console.error(error);
        res.status(500).json({message: error.message, ...getStandardErrorFields(req.path)})
    });
}

exports.changeState = function (req, res, next) {
    console.log(`Changing id ${req.params.id} state to ${req.body.state}`);
    Todo.findByIdAndUpdate(req.params.id, {state: req.body.state}, {select: selectedFields})
        .then(doc => {
            if (doc) {
                console.log("Found TODO to update");
                res.status(200).json({
                    message: "TODO was updated successfully",
                    body: doc
                });
            } else {
                todoNotFoundResponse(res)
            }
        }).catch(error => {
            console.error(error);
            res.status(500).json({message: error.message, ...getStandardErrorFields(req.path)})
        }
    );
}


exports.delete = function (req, res, next) {

    console.log(`Deleting id ${req.params.id}`);
    Todo.findByIdAndDelete(req.params.id, {state: req.body.state, select: selectedFields})
        .then(doc => {
            if (doc) {
                console.log("Found TODO to delete");
                let pathOfImageToDelete = doc.attachment ? doc.attachment.path : undefined;
                pathOfImageToDelete ? fs.unlinkSync(pathOfImageToDelete) :
                    console.log('No attachment was found on document');
                res.status(200).json({
                    message: `Todo was deleted successfully`,
                    body: doc
                });
            } else {
                todoNotFoundResponse(res);
            }
        }).catch(error => {
            console.error(error);
            res.status(500).json({message: error.message, ...getStandardErrorFields(req.path)})
        }
    );
}

