const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

let TodoSchema = new Schema({
    description: String,
    state: String,
    attachment: new Schema({
        name: {type:String, required: false},
        path: {type: String, required: false},
        contentType: {type: String, required: false, max: 15},
    }, {timestamps: true}),
}, {timestamps: true});

autoIncrement.initialize(mongoose.connection);
TodoSchema.plugin(autoIncrement.plugin, 'Todo');

module.exports = {Todo: mongoose.model('Todo', TodoSchema),}