const express = require('express');
const router = express.Router();
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/files')
    },
    filename: function (req, file, cb) {
        let fileName = file.originalname.split('.');
        cb(null, `${fileName[0]}-${new Date().getTime()}.${fileName[fileName.length -1 ]}`);
    }
})
const upload = multer({storage: storage});


const todoController = require('../controllers/todo.controller');
const todoMiddleware = require('../middlewares/todo.middleware');

router.post('/', [ upload.single('todoAttachment'), todoMiddleware.checkState, todoMiddleware.checkDescription], todoController.create);
router.get('/', todoController.get);
router.get('/attachment/:id', todoController.getAttachment);
router.get('/:id', todoMiddleware.checkID, todoController.get);
router.delete('/:id', todoController.delete);
router.put('/:id', [todoMiddleware.checkState, todoMiddleware.checkID], todoController.changeState);

module.exports = router;
