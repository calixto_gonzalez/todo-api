### Installation
First install docker from here [Docker docs](https://docs.docker.com/) follow the instructions for you OS, check your docker installation with `docker --version`
if everything is correct you should see something like this: 

![Docker image](https://i.gyazo.com/a4d6f5971885bb849e63a11dd18183ee.png)

After you have cloned the repository open your terminal there and do `cd todo-api && docker-compose build && docker-compose up` if you want to stop the application do Ctrl+C and it should stop.
### Endpoints
For the endpoints you should download my Postman Collection to try them out [Postman Collection](https://www.getpostman.com/collections/5154819a8e164e0da6e5)
and you will also need this JSON for the variables

`{
 	"id": "38226ff8-0dfa-4311-8880-1ce030e05b8d",
 	"name": "todo-api",
 	"values": [
 		{
 			"key": "url",
 			"value": "localhost:3000",
 			"enabled": true
 		},
 		{
 			"key": "id",
 			"value": "0",
 			"enabled": true
 		},
 		{
 			"key": "state",
 			"value": "pending",
 			"enabled": true
 		},
 		{
 			"key": "description",
 			"value": "some description",
 			"enabled": true
 		}
 	],
 	"_postman_variable_scope": "environment",
 	"_postman_exported_at": "2020-03-12T01:55:14.284Z",
 	"_postman_exported_using": "Postman/7.20.0"
 }`
 
 save the json to a file and import from Postman for more information [Postman Docs](https://learning.postman.com/docs/postman/collections/data-formats/#importing-postman-data)
 
 ### Table of endpoints
 
 | Endpoints     | Method |   Body   | Query params | Aditional information|
 | ------------- | ------------- | ----------- | --------- | -----------|
 | `/`  |   POST| form data with **description**, **state** and **todoAttachment** | | if state is present it should be **done** or **pending**|
 | `/`  | GET  | | **description**, **id** and **state** | it is an and for all parameters so if description and id is present it will filter if both match|
|`/:id`|GET| |id required|Gets TODO from server|
|`/:id`|DELETE| |id required|Deletes TODO from server|
|`/:id`|PUT| state should be in the body in JSON|id required| Updates TODO state|
|`/attachment/:id`|GET| |id required|Gets file from server|

## Examples
Some examples with cURL

POST TODO example

`curl --location --request POST 'localhost:3000/todo/' --header 'Content-Type: multipart/form-data' --form 'todoAttachment=@/c/Users/yasir/Pictures/some_picture.jpg' --form 'state=pending' --form 'description=this is clearly working'`

response:

`{"message":"Created TODO successfully","body":{"todo":{"description":"this is clearly working","state":"pending","attachment":{"_id":"5e69a108b7b5a843587817fd","name":"some_picture-1583980798065.jpg","path":"public\\images\\some_picture-1583980798065.jpg","contentType":"image/jpeg","createdAt":"2020-03-12T02:40:09.089Z","updatedAt":"2020-03-12T02:40:09.089Z"},"createdAt":"2020-03-12T02:40:09.089Z","updatedAt":"2020-03-12T02:40:09.089Z","_id":32,"__v":0}}}`

GET All TODOs

`curl --location --request GET 'localhost:3000/todo/'`

response

`{
   "body": {
     "count": 2,
     "todos": [
       {
         "_id": 2,
         "description": "un bebe baybe",
         "state": "done",
         "attachment": {
           "path": "public\\images\\roberto-delgado-webb-AxI9niqj_60-unsplash-1583951606383.jpg",
           "contentType": "image/jpeg",
           "createdAt": "2020-03-11T18:33:26.420Z"
         },
         "createdAt": "2020-03-11T18:33:26.421Z"
       },
       {
         "_id": 6,
         "description": "una puta",
         "state": "done",
         "attachment": {
           "path": "public\\images\\averie-woodard-Av_NirIguEc-unsplash-1583951631145.jpg",
           "contentType": "image/jpeg",
           "createdAt": "2020-03-11T18:33:51.154Z"
         },
         "createdAt": "2020-03-11T18:33:51.154Z"
       }
     ]
   }
 }`
 
 GET TODOs with filters
 
 `curl --location --request GET 'localhost:3000/todo/?description=some%20description'`
 
 response:
 
`{
     "body": {
         "message": "TODO not found"
     }
 }`
 
in this last example there were no TODOs matching that description.